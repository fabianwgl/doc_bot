const webDriver = require('selenium-webdriver')
const seleniumDrivers = require('selenium-drivers')

let driver
seleniumDrivers.init({
  browserName: 'chrome'
}).then(() => {
  driver = new webDriver.Builder()
    .forBrowser('chrome')
    .build()
})
module.exports = robot => {

  robot.respond(/pone la radio/i, res => {
    if(driver){
      res.send('ya calmao')
      driver.get('http://playfm.cl/')
      .then(() => {
        driver.sleep(15000)
        .then(() => {
          driver.findElement(webDriver.By.id('janusPlay'))
          .then((elem) => {
            elem.click()
            res.send('listo :thumbsup:')
          })
        })
      })
    }else{
      res.send('no hay driver loco ayuda pls')
    }
  })

  robot.respond(/apaga la radio/i, res => {
    if(driver){
      driver.quit()
      res.send('listo :thumbsup:')
    }else{
      res.send('cual radio wm')
    }
  })

  robot.respond(/pone este video (.*)/i, res => {
    if(driver){
      let url = res.match[1]
      driver.get(url)
      .then(() => {
        res.send('listo :thumbsup:')
      })
    }
  })

}


